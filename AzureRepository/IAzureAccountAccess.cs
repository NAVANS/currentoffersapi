﻿using Microsoft.WindowsAzure.Storage.Table;

namespace AzureRepository
{
    public interface IAzureAccountAccess
    {
        CloudTable GetAzureTableReference(string accountName, string accountKey, string tableName);
    }
}
