﻿using Microsoft.WindowsAzure.Storage;
using Microsoft.WindowsAzure.Storage.Auth;
using Microsoft.WindowsAzure.Storage.Table;

namespace AzureRepository
{
    public class AzureAccountAccess:IAzureAccountAccess
    {
        public CloudTable GetAzureTableReference(string accountName, string accountKey,string tableName)
        {
            try
            {
                StorageCredentials credentials = new StorageCredentials(accountName, accountKey);

                CloudStorageAccount account = new CloudStorageAccount(credentials, useHttps: true);
                CloudTableClient client = account.CreateCloudTableClient();
                CloudTable table = client.GetTableReference(tableName);

                return table;
            }
            catch
            {
                return null;
            }
        }
    }
}
