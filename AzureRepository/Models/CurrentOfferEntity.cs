﻿using Microsoft.WindowsAzure.Storage.Table;

namespace AzureRepository.Models
{
    public class CurrentOfferEntity : TableEntity
    {
        public string VehicleSubtitle { get; set; }
        public string VehicleTitle { get; set; }
        public string OfferSubtitle { get; set; }
        public string OfferTitle { get; set; }
        public bool IsActive { get; set; }
        public string Disclaimers { get; set; }
    }
}
