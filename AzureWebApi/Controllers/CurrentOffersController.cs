﻿using AzureRepository.Models;
using AzureServices;
using System.Collections.Generic;
using System.Threading.Tasks;
using System.Web.Http;

namespace AzureWebApi.Controllers
{
    [RoutePrefix("api/currentoffers")]
    public class CurrentOffersController : ApiController
    {
        private readonly ICurrentOffersServices _currentOffersSvc = new CurrentOffersServices();

        [HttpGet]
        [Route("Get/{rowKey}")]
        public async Task<CurrentOfferEntity> Get(string rowKey)
        {
            var entity = new CurrentOfferEntity
            {
                PartitionKey = "Current Offer",
                RowKey = rowKey
            };

            var response = await _currentOffersSvc.GetCurrentOffer(entity);

            return response;
        }

        [HttpPost]
        [Route("Insert")]
        public async Task<CurrentOfferEntity> Post(CurrentOfferEntity entity)
        {
            var response = await _currentOffersSvc.InsertCurrentOffer(entity);

            return response;
        }


        [HttpPost]
        [Route("BatchInsert")]
        public async Task<IHttpActionResult> PostAll(List<CurrentOfferEntity> entities)
        {
            var response = await _currentOffersSvc.BatchInsertCurrentOffers(entities);

            if (response == false) return InternalServerError();

            return Ok(true);
        }

        // PUT api/values/5
        public void Put(int id, [FromBody] string value)
        {
        }

        [HttpDelete]
        [Route("Delete/{rowKey}")]
        public async Task<IHttpActionResult> Delete(string rowKey)
        {
            var entity = new CurrentOfferEntity
            {
                PartitionKey = "Current Offer",
                RowKey = rowKey
            };

            var response = await _currentOffersSvc.DeleteCurrentOffer(entity);

            if (response == false) return NotFound();

            return Ok(true);
        }
    }

}
