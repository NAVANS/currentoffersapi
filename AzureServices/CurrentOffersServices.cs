﻿using AzureRepository;
using AzureRepository.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace AzureServices
{
    public class CurrentOffersServices:ICurrentOffersServices
    {
        private readonly IAzureAccountAccess _iAzureAccountAccess = new AzureAccountAccess();
        private const string AccountName = "currentoffers";
        private const string AccountKey = "t94PTB58iWjenXh/ngWbEcVycrgFfLz+fDOYdgbfB1UOtM/tA3EG3Iv4lr38iEX/kFXdbvIViB++EJGVKdEBfQ==";
        private readonly CloudTable _table;

        public CurrentOffersServices()
        {
            _table = _iAzureAccountAccess.GetAzureTableReference(AccountName, AccountKey,"CurrentOffers");
        }

        public async Task<CurrentOfferEntity> InsertCurrentOffer(TableEntity entity)
        {

            try
            {
                TableOperation insertOperation = TableOperation.InsertOrReplace(entity);

                var result = await _table.ExecuteAsync(insertOperation);

                CurrentOfferEntity currentOfferEntity = result.Result as CurrentOfferEntity;

                return currentOfferEntity;
            }
            catch (Exception e)
            {
                //log error
                throw e;
            }

        }

        public async Task<bool> BatchInsertCurrentOffers(List<CurrentOfferEntity> tableEntities)
        {

            try
            {
                foreach (var insertOperation in tableEntities.Select(TableOperation.InsertOrReplace))
                {
                    await _table.ExecuteAsync(insertOperation);
                }

                return true;
            }
            catch (Exception e)
            {
                //log error
                return false;
            }

        }

        public async Task<bool> DeleteCurrentOffer(TableEntity tableEntity)
        {
            try
            {
                tableEntity.ETag = "*";

                TableOperation entity = TableOperation.Delete(tableEntity);

                var result = await _table.ExecuteAsync(entity);

                return result.Result != null;
            }
            catch (Exception e)
            {
                //log error
                return false;
            }
        }

        public async Task<CurrentOfferEntity> GetCurrentOffer(TableEntity tableEntity)
        {
            try
            {
                TableOperation entity = TableOperation.Retrieve<CurrentOfferEntity>(tableEntity.PartitionKey, tableEntity.RowKey);

                var result = await _table.ExecuteAsync(entity);

                CurrentOfferEntity currentOfferEntity = result.Result as CurrentOfferEntity;

                return currentOfferEntity;
            }
            catch (Exception e)
            {
                //log error
                return null;
            }

        }
    }
}
