﻿using AzureRepository.Models;
using Microsoft.WindowsAzure.Storage.Table;
using System.Collections.Generic;
using System.Threading.Tasks;

namespace AzureServices
{
    public interface ICurrentOffersServices
    {
        Task<CurrentOfferEntity> InsertCurrentOffer(TableEntity tableEntity);
        Task<bool> DeleteCurrentOffer(TableEntity tableEntity);
        Task<CurrentOfferEntity> GetCurrentOffer(TableEntity tableEntity);
        Task<bool> BatchInsertCurrentOffers(List<CurrentOfferEntity> tableEntities);
    }
}
